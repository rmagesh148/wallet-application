class User < ActiveRecord::Base
  has_many :categories
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  def total_income
    total = 0
    categories.each do |category|
      total += category.incomes_total
    end
    total
  end
  
  def total_cost
    total = 0
    categories.each do |category|
      total += category.costs_total
    end
    total
  end
  
  def total_balance
    self.total_income + self.total_cost
  end
end
