class CostsController < ApplicationController
  before_action :set_cost, only: [:show, :edit, :update, :destroy]

  # GET /costs
  # GET /costs.json
  def index
    @costs = Cost.all
  end

  # GET /costs/1
  # GET /costs/1.json
  def show
  end

  # GET /costs/new
  def new
    @cost = Cost.new
  end

  # GET /costs/1/edit
  def edit
  end

  # POST /costs
  # POST /costs.json
  def create
    @category = Category.find(params[:category_id])
    @cost   = Cost.new(cost_params.merge(category_id: params[:category_id]))
    if @cost.save
      respond_to do |format|
        format.html { redirect_to @category, notice: 'Cost was successfully created.' }
      end
    else
      
      render "categories/show"
    end
  end

  # PATCH/PUT /costs/1
  # PATCH/PUT /costs/1.json
  def update
    respond_to do |format|
      if @cost.update(cost_params)
        format.html { redirect_to @cost, notice: 'Cost was successfully updated.' }
        format.json { render :show, status: :ok, location: @cost }
      else
        format.html { render :edit }
        format.json { render json: @cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /costs/1
  # DELETE /costs/1.json
  def destroy
    cost = Cost.find(params["id"])
    @category = Category.find(params[:category_id])
    cost.destroy
    respond_to do |format|
      format.html { redirect_to categories_path, notice: 'Cost was successfully deleted.' }
    end 
  end
  


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cost
      @cost = Cost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cost_params
      params.require(:cost).permit(:amount, :category_id)
    end
end
